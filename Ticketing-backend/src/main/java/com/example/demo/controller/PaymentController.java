package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.payloads.PaymentDto;
import com.example.demo.service.PaymentService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class PaymentController {
	
    @Autowired
    PaymentService paymentService;
    
    @PostMapping("/payment/{userId}")
	public ResponseEntity<PaymentDto> createPayment(
			@RequestBody PaymentDto paymentDto,
			@PathVariable Long userId)
		
	
	{
		PaymentDto createPayment = this.paymentService.createPayment(paymentDto,userId);
		return new ResponseEntity<PaymentDto>(createPayment,HttpStatus.CREATED);
	}
}
