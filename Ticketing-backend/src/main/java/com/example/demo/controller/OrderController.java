package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.payloads.ApiResponse;
import com.example.demo.payloads.OrderDto;
import com.example.demo.payloads.TicketDto;
import com.example.demo.service.OrderService;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class OrderController {
	
	@Autowired
	OrderService orderService;
	
	//create
	@PostMapping("/orders/{userId}")
	public ResponseEntity<OrderDto> createOrder(
			@RequestBody OrderDto orderDto,
			@PathVariable Long userId)
		
	
	{
		OrderDto createOrder = this.orderService.createOrder(orderDto,userId);
		return new ResponseEntity<OrderDto>(createOrder,HttpStatus.CREATED);
	}
	//delete
	@DeleteMapping("/{orderId}")
	public ResponseEntity<ApiResponse> deleteOrder(@PathVariable("orderId") Integer oid){
		this.deleteOrder(oid);
		return new ResponseEntity<ApiResponse>(new ApiResponse("Order deleted successfully",true),HttpStatus.OK);
	}

    // all orders
	@GetMapping("/orders")
	public ResponseEntity<List<OrderDto>> getAllOrder()
	{
		List<OrderDto> allOrder = this.orderService.getAllOrder();
		return new ResponseEntity<List<OrderDto>>(allOrder,HttpStatus.OK);
		
	}
	
	//get order by id
	@GetMapping("/orders/{orderId}")
	public ResponseEntity<OrderDto> getOrderById(@PathVariable Integer orderId )
	{
		OrderDto orderDto=this.orderService.getOrderById(orderId);
		return new ResponseEntity<OrderDto>(orderDto,HttpStatus.OK);
		
	}
	
	@GetMapping("/order/{userId}")
	public ResponseEntity<List<OrderDto>> getOrderByUserId(
			@PathVariable Long userId)
	{
		List<OrderDto> tickets=this.orderService.getOrderByUserId(userId);
	  return new ResponseEntity<List<OrderDto>>(tickets,HttpStatus.OK);
	}
	
	
}
