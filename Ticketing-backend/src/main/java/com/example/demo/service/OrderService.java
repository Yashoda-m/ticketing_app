package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.payloads.OrderDto;
import com.example.demo.payloads.TicketDto;


public interface OrderService {
	
	//create
	OrderDto createOrder(OrderDto orderDto,Long userId);
	
	//order by id
	OrderDto getOrderById(Integer orderId);
	
	//get all orders
	List<OrderDto> getAllOrder();
	
	//delete order
	void deleteOrder(Integer orderId);
	
	//get all orders by user
		List<OrderDto> getOrderByUserId(Long userId);

}
