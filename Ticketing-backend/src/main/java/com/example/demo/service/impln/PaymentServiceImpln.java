package com.example.demo.service.impln;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Order;
import com.example.demo.entity.Payment;
import com.example.demo.entity.User;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.payloads.PaymentDto;
import com.example.demo.repository.OrderRepo;
import com.example.demo.repository.PaymentRepo;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.PaymentService;

@Service
public class PaymentServiceImpln implements PaymentService {

	@Autowired
	private UserRepository userRepo;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private PaymentRepo paymentRepo;
	
	@Override
	public PaymentDto createPayment(PaymentDto paymentDto, Long userId) {
        User user=this.userRepo.findById(userId).orElseThrow(()->new ResourceNotFoundException("User","User id",userId));
		
		Payment payment=this.modelMapper.map(paymentDto, Payment.class);
		payment.setUser(user);
		
		Payment newPayment=this.paymentRepo.save(payment);
		return this.modelMapper.map(newPayment,PaymentDto.class);
	}

}
