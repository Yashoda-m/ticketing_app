package com.example.demo.service.impln;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Order;
import com.example.demo.entity.Ticket;
import com.example.demo.entity.User;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.payloads.OrderDto;
import com.example.demo.payloads.TicketDto;
import com.example.demo.repository.OrderRepo;
import com.example.demo.repository.TicketRepo;
import com.example.demo.repository.UserRepository;
import com.example.demo.service.OrderService;
@Service
public class OrderServiceImpln implements OrderService{

	@Autowired
	private TicketRepo ticketRepo;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private UserRepository userRepo;
	
	@Autowired
	private OrderRepo orderRepo;
	@Override
	public OrderDto createOrder(OrderDto orderDto, Long userId) {
        User user=this.userRepo.findById(userId).orElseThrow(()->new ResourceNotFoundException("User","User id",userId));
		
		Order order=this.modelMapper.map(orderDto,Order.class);
		order.setUser(user);
		
		Order newOrder=this.orderRepo.save(order);
		return this.modelMapper.map(newOrder,OrderDto.class);
	}

	@Override
	public OrderDto getOrderById(Integer orderId) {
	 Order order=this.orderRepo.findById(orderId).orElseThrow(()->new ResourceNotFoundException("Order","order id",orderId));
		return this.modelMapper.map(order, OrderDto.class);
		
	}

	@Override
	public List<OrderDto> getAllOrder() {
		List<Order> allOrders = this.orderRepo.findAll();
		List<OrderDto> orderDtos=allOrders.stream().map((order)->this.modelMapper.map(order, OrderDto.class))
				.collect(Collectors.toList());
		return orderDtos;
		
	}

	@Override
	public void deleteOrder(Integer orderId) {
			Order order=this.orderRepo.findById(orderId)
			.orElseThrow(()->new ResourceNotFoundException("Order","id",orderId));
			this.orderRepo.delete(order);
		}

	@Override
	public List<OrderDto> getOrderByUserId(Long userId) {
		User user=this.userRepo.findById(userId).orElseThrow(()->new ResourceNotFoundException("User","User id",userId));
		List<Order> orders=this.orderRepo.findByUser(user);
		List<OrderDto> orderDtos=orders.stream().map((order)->this.modelMapper.map(order, OrderDto.class)).collect(Collectors.toList());
		return orderDtos;
	}
	

		
	}


