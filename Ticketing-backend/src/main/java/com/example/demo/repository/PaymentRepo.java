package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Order;
import com.example.demo.entity.Payment;


public interface PaymentRepo extends JpaRepository<Payment , Integer>{

	List<Payment> findByOrder(Order order); 

}
