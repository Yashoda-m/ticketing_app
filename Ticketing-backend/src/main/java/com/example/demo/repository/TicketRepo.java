package com.example.demo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Ticket;
import com.example.demo.entity.User;

public interface TicketRepo extends JpaRepository<Ticket , Integer> {

	
	List<Ticket> findByUser(User user);
}
