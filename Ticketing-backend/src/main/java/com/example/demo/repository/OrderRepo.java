package com.example.demo.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.entity.Order;
import com.example.demo.entity.Ticket;
import com.example.demo.entity.User;

public interface OrderRepo extends JpaRepository<Order , Integer>{

	List<Order> findByTicket(Ticket ticket);
	List<Order> findByUser(User user);
}
