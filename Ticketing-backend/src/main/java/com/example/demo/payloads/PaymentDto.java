package com.example.demo.payloads;

import java.util.Date;

import com.example.demo.entity.Order;
import com.example.demo.entity.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class PaymentDto {
	private int paymentid;
	//private String status;
	private Long cardno;
	private Date expirydate;
	private Order order;
	private User user;
}
