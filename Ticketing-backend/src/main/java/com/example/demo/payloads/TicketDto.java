package com.example.demo.payloads;



import com.example.demo.entity.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class TicketDto {

	private int ticketid;
	private String title;
	private double price;
	private int quantity;
	private User user;
}
