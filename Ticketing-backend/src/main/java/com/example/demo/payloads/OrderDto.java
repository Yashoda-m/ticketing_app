package com.example.demo.payloads;

import com.example.demo.entity.Ticket;
import com.example.demo.entity.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OrderDto {
	private int orderid;
	private String status;
	private Ticket ticket;
	private User user;
}
