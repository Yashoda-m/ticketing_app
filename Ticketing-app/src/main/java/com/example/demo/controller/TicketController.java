package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.demo.entity.Ticket;
import com.example.demo.entity.User;
import com.example.demo.payloads.TicketDto;
import com.example.demo.service.TicketService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api")
public class TicketController {
	
	@Autowired
	private TicketService ticketService;
	//create
	@PostMapping("/users/{userId}/tickets")
	public ResponseEntity<TicketDto> createTicket(
			@RequestBody TicketDto ticketDto,
			@PathVariable Long userId)
		
	
	{
		TicketDto createTicket = this.ticketService.createTicket(ticketDto,userId);
		return new ResponseEntity<TicketDto>(createTicket,HttpStatus.CREATED);
	}
  // get all tickets
	@GetMapping("/tickets")
	public ResponseEntity<List<TicketDto>> getAllTicket()
	{
		List<TicketDto> allTicket = this.ticketService.getAllTicket();
		return new ResponseEntity<List<TicketDto>>(allTicket,HttpStatus.OK);
		
	}
	
	//get ticket details by id
	@GetMapping("/tickets/{ticketId}")
	public ResponseEntity<TicketDto> getTicketById(@PathVariable Integer ticketId )
	{
		TicketDto ticketDto=this.ticketService.getTicketById(ticketId);
		return new ResponseEntity<TicketDto>(ticketDto,HttpStatus.OK);
		
	}
	//update ticket Price
	@PutMapping("/tickets/{ticketId}")
	public ResponseEntity<TicketDto> updateTicket(@RequestBody TicketDto ticketDto,@PathVariable Integer ticketId){
		TicketDto updateTicket=this.ticketService.updateTicket(ticketDto,ticketId);
		return new ResponseEntity<TicketDto>(updateTicket,HttpStatus.OK);
	}
	
}

