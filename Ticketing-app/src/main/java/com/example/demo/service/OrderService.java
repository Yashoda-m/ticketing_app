package com.example.demo.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.payloads.OrderDto;


public interface OrderService {
	
	//create
	OrderDto createOrder(OrderDto orderDto,Integer ticketId);
	
	//order by id
	OrderDto getOrderById(Integer orderId);
	
	//get all orders
	List<OrderDto> getAllOrder();
	
	//delete order
	void deleteOrder(Integer orderId);

}
