package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.Ticket;
import com.example.demo.payloads.TicketDto;

public interface TicketService {

	//create
	TicketDto createTicket(TicketDto ticketDto,Long userId);
	
	//update
	TicketDto updateTicket(TicketDto ticketDto, Integer ticketId );
	
	
	//get all tickets
	
	List<TicketDto> getAllTicket();
	
	//get single ticket
	TicketDto getTicketById(Integer ticketId);
	
	//get all tickets by user
	//List<TicketDto> getTicketByUser(Integer user);
}
