package com.example.demo.service.impln;

import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Ticket;
import com.example.demo.entity.User;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.payloads.TicketDto;
import com.example.demo.repository.TicketRepo;

import com.example.demo.repository.UserRepository;
import com.example.demo.service.TicketService;

@Service
public class TicketServiceImpln implements TicketService {

	@Autowired
	private TicketRepo ticketRepo;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private UserRepository userRepo;
	
	@Override
	public 	TicketDto createTicket(TicketDto ticketDto,Long userId){
		
		User user=this.userRepo.findById(userId).orElseThrow(()->new ResourceNotFoundException("User","User id",userId));
		
		Ticket ticket=this.modelMapper.map(ticketDto, Ticket.class);
		ticket.setUser(user);
		
		Ticket newTicket=this.ticketRepo.save(ticket);
		return this.modelMapper.map(newTicket,TicketDto.class);
	}

	@Override
	public TicketDto updateTicket(TicketDto ticketDto, Integer ticketId) {
		Ticket ticket=this.ticketRepo.findById(ticketId).orElseThrow(()->new ResourceNotFoundException("Ticket","ticket id",ticketId));
		//ticket.setTitle(ticketDto.getTitle());
		ticket.setPrice(ticketDto.getPrice());
		//ticket.setQuantity(ticketDto.getQuantity());
		Ticket updatedTicket=this.ticketRepo.save(ticket);
		return this.modelMapper.map(updatedTicket, TicketDto.class);
	}

	@Override
	public List<TicketDto> getAllTicket() {
		List<Ticket> allTickets = this.ticketRepo.findAll();
		List<TicketDto> ticketDtos=allTickets.stream().map((ticket)->this.modelMapper.map(ticket, TicketDto.class))
				.collect(Collectors.toList());
		return ticketDtos;
	}

	@Override
	public TicketDto getTicketById(Integer ticketId) {
		Ticket ticket=this.ticketRepo.findById(ticketId).orElseThrow(()->new ResourceNotFoundException("Ticket","ticket id",ticketId));
		return this.modelMapper.map(ticket, TicketDto.class);
	}

	

}
