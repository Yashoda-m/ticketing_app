package com.example.demo.service.impln;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Order;
import com.example.demo.entity.Payment;
import com.example.demo.exceptions.ResourceNotFoundException;
import com.example.demo.payloads.PaymentDto;
import com.example.demo.repository.OrderRepo;
import com.example.demo.repository.PaymentRepo;
import com.example.demo.service.PaymentService;

@Service
public class PaymentServiceImpln implements PaymentService {

	@Autowired
	private OrderRepo orderRepo;
	@Autowired
	private ModelMapper modelMapper;
	@Autowired
	private PaymentRepo paymentRepo;
	
	@Override
	public PaymentDto createPayment(PaymentDto paymentDto, Integer orderId) {
        Order order=this.orderRepo.findById(orderId).orElseThrow(()->new ResourceNotFoundException("Order","Order id",orderId));
		
		Payment payment=this.modelMapper.map(paymentDto, Payment.class);
		payment.setOrder(order);
		
		Payment newPayment=this.paymentRepo.save(payment);
		return this.modelMapper.map(newPayment,PaymentDto.class);
	}

}
