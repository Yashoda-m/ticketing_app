package com.example.demo.payloads;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;


@AllArgsConstructor
@NoArgsConstructor
@Data
public class ApiResponse {

	private String message;
	private boolean success;
}
