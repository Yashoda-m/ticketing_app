package com.example.demo.entity;



import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name="Ticket")
public class Ticket {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private int ticketid;
	private String title;
	private double price;
	private int quantity;
	@ManyToOne
	private User user;
	
	

}
