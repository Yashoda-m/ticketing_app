package com.example.demo.entity;
import jakarta.persistence.*;

@Entity
@Table(name = "roles")
public class Role {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Enumerated(EnumType.STRING)
    @Column(length = 20)
    private Role_Type name;

    public Role() {

    }

    public Role(Role_Type name) {
        this.name = name;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Role_Type getName() {
        return name;
    }

    public void setName(Role_Type name) {
        this.name = name;
    }

}
