package com.example.demo.repository;

import com.example.demo.entity.Role;
import com.example.demo.entity.Role_Type;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role,Long> {
    Optional<Role> findByName(Role_Type name);
}
